!(function ($) {
    "use strict";

    // Toggle .header-scrolled class to #header when page is scrolled
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#header').addClass('header-scrolled');
        } else {
            $('#header').removeClass('header-scrolled');
        }
    });

    if ($(window).scrollTop() > 100) {
        $('#header').addClass('header-scrolled');
    }

    // Stick the header at top on scroll
    $("#header").sticky({
        topSpacing: 0,
        zIndex: '50'
    });

    // Smooth scroll for the navigation menu and links with .scrollto classes
    $(document).on('click', '.nav-menu a, .mobile-nav a, .scrollto', function (e) {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            e.preventDefault();
            var target = $(this.hash);
            if (target.length) {

                var scrollto = target.offset().top;
                var scrolled = 2;

                if ($('#header-sticky-wrapper').length) {
                    scrollto -= $('#header-sticky-wrapper').outerHeight() - scrolled;
                }

                if ($(this).attr("href") == '#header') {
                    scrollto = 0;
                }

                $('html, body').animate({
                    scrollTop: scrollto
                }, 1500, 'easeInOutExpo');

                if ($(this).parents('.nav-menu, .mobile-nav').length) {
                    $('.nav-menu .active, .mobile-nav .active').removeClass('active');
                    $(this).closest('li').addClass('active');
                }

                if ($('body').hasClass('mobile-nav-active')) {
                    $('body').removeClass('mobile-nav-active');
                    $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
                    $('.mobile-nav-overly').fadeOut();
                }
                return false;
            }
        }
    });

    // Mobile Navigation
    if ($('.nav-menu').length) {
        var $mobile_nav = $('.nav-menu').clone().prop({
            class: 'mobile-nav d-lg-none'
        });
        $('body').append($mobile_nav);
        $('body').prepend('<button type="button" class="mobile-nav-toggle d-lg-none"><i class="icofont-navigation-menu"></i></button>');
        $('body').append('<div class="mobile-nav-overly"></div>');

        $(document).on('click', '.mobile-nav-toggle', function (e) {
            $('body').toggleClass('mobile-nav-active');
            $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
            $('.mobile-nav-overly').toggle();
        });

        $(document).on('click', '.mobile-nav .drop-down > a', function (e) {
            e.preventDefault();
            $(this).next().slideToggle(300);
            $(this).parent().toggleClass('active');
        });

        $(document).click(function (e) {
            var container = $(".mobile-nav, .mobile-nav-toggle");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                if ($('body').hasClass('mobile-nav-active')) {
                    $('body').removeClass('mobile-nav-active');
                    $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
                    $('.mobile-nav-overly').fadeOut();
                }
            }
        });
    } else if ($(".mobile-nav, .mobile-nav-toggle").length) {
        $(".mobile-nav, .mobile-nav-toggle").hide();
    }

    // Navigation active state on scroll
    var nav_sections = $('section');
    var main_nav = $('.nav-menu, #mobile-nav');
    var main_nav_height = $('#header').outerHeight();

    $(window).on('scroll', function () {
        var cur_pos = $(this).scrollTop() + 10;

        nav_sections.each(function () {
            var top = $(this).offset().top - main_nav_height,
                bottom = top + $(this).outerHeight();

            if (cur_pos >= top && cur_pos <= bottom) {
                if (cur_pos <= bottom) {
                    main_nav.find('li').removeClass('active');
                }
                main_nav.find('a[href="#' + $(this).attr('id') + '"]').parent('li').addClass('active');
            }
        });
    });

    // Intro carousel
    var heroCarousel = $("#heroCarousel");
    var heroCarouselIndicators = $("#hero-carousel-indicators");
    heroCarousel.find(".carousel-inner").children(".carousel-item").each(function (index) {
        (index === 0) ?
            heroCarouselIndicators.append("<li data-target='#heroCarousel' data-slide-to='" + index + "' class='active'></li>") :
            heroCarouselIndicators.append("<li data-target='#heroCarousel' data-slide-to='" + index + "'></li>");
    });

    heroCarousel.on('slid.bs.carousel', function (e) {
        $(this).find('h2').addClass('animated fadeInDown');
        $(this).find('p').addClass('animated fadeInUp');
        $(this).find('.btn-get-started').addClass('animated fadeInUp');
    });

    // Back to top button
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.back-to-top').fadeIn('slow');
        } else {
            $('.back-to-top').fadeOut('slow');
        }
    });

    $('.back-to-top').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 1500, 'easeInOutExpo');
        return false;
    });

    // Initi AOS
    AOS.init({
        duration: 1000,
        easing: "ease-in-out-back"
    });

    // Auto-scroll
    $('#resource-carousel').carousel({
        interval: 0
    });

    // Control buttons
    $('.next').click(function () {
        $('.carousel').carousel('next');
        return false;
    });

    // On carousel scroll
    $("#resource-carousel").on("slide.bs.carousel", function (e) {
        var $e = $(e.relatedTarget);
        var idx = $e.index();
        var itemsPerSlide = 3;
        var totalItems = $(".carousel-item").length;
        if (idx >= totalItems - (itemsPerSlide - 1)) {
            var it = itemsPerSlide -
                (totalItems - idx);
            for (var i = 0; i < it; i++) {
                // append slides to end 
                if (e.direction == "left") {
                    $(".carousel-item").eq(i).appendTo(".carousel-inner");
                } else {
                    $(".carousel-item").eq(0).appendTo(".carousel-inner");
                }
            }
        }
    });

})(jQuery);

//function for get a quote
function get_quote(id) {

    if (id == 'biomarket') { //bioanalytical module
        document.getElementById("module").options[1].selected = true;
        document.getElementById("component").options[5].selected = true;
    } else if (id == 'pk') { //bioanalytical module
        document.getElementById("module").options[1].selected = true;
        document.getElementById("component").options[4].selected = true;
    } else if (id == 'trending') { //bioanalytical module
        document.getElementById("module").options[1].selected = true;
        document.getElementById("component").options[3].selected = true;
    } else if (id == 'ada') { //bioanalytical module
        document.getElementById("module").options[1].selected = true;
        document.getElementById("component").options[1].selected = true;
    } else if (id == 'biosimilar') { //bioanalytical module
        document.getElementById("module").options[1].selected = true;
        document.getElementById("component").options[2].selected = true;
    } else if (id == 'inventory') { //productivity module
        document.getElementById("module").options[2].selected = true;
        document.getElementById("component").options[6].selected = true;
    } else if (id == 'rfid') { //productivity module
        document.getElementById("module").options[2].selected = true;
        document.getElementById("component").options[7].selected = true;
    } else if (id == 'e-worksheet') { //productivity module
        document.getElementById("module").options[2].selected = true;
        document.getElementById("component").options[8].selected = true;
    } else {
        document.getElementById("module").options[0].selected = true;
        document.getElementById("component").options[0].selected = true;
    }
}

//function for redirect to bioanalytical component details
function redirect(id) {

    if (id == 'rm-biomarker') {
        var addClass = document.getElementById("bio-sub-item-3");
        addClass.classList.add("active");
        var removeClass_1st = document.getElementById("bio-sub-item-4");
        removeClass_1st.classList.remove("active");
        var removeClass_2nd = document.getElementById("bio-sub-item-5");
        removeClass_2nd.classList.remove("active");
        var removeClass_3rd = document.getElementById("bio-sub-item-1");
        removeClass_3rd.classList.remove("active");
        var removeClass_4th = document.getElementById("bio-sub-item-2");
        removeClass_4th.classList.remove("active");
    }
    else if (id == 'rm-ada') {
        var addClass = document.getElementById("bio-sub-item-1");
        addClass.classList.add("active");
        var removeClass_1st = document.getElementById("bio-sub-item-2");
        removeClass_1st.classList.remove("active");
        var removeClass_2nd = document.getElementById("bio-sub-item-3");
        removeClass_2nd.classList.remove("active");
        var removeClass_3rd = document.getElementById("bio-sub-item-4");
        removeClass_3rd.classList.remove("active");
        var removeClass_4th = document.getElementById("bio-sub-item-5");
        removeClass_4th.classList.remove("active");
    } else if (id == 'rm-biosimilar') {
        var addClass = document.getElementById("bio-sub-item-4");
        addClass.classList.add("active");
        var removeClass_1st = document.getElementById("bio-sub-item-5");
        removeClass_1st.classList.remove("active");
        var removeClass_2nd = document.getElementById("bio-sub-item-1");
        removeClass_2nd.classList.remove("active");
        var removeClass_3rd = document.getElementById("bio-sub-item-2");
        removeClass_3rd.classList.remove("active");
        var removeClass_4th = document.getElementById("bio-sub-item-3");
        removeClass_4th.classList.remove("active");
    }
    else if (id == 'rm-pk') {
        var addClass = document.getElementById("bio-sub-item-2");
        addClass.classList.add("active");
        var removeClass_1st = document.getElementById("bio-sub-item-3");
        removeClass_1st.classList.remove("active");
        var removeClass_2nd = document.getElementById("bio-sub-item-4");
        removeClass_2nd.classList.remove("active");
        var removeClass_3rd = document.getElementById("bio-sub-item-5");
        removeClass_3rd.classList.remove("active");
        var removeClass_4th = document.getElementById("bio-sub-item-1");
        removeClass_4th.classList.remove("active");
    } else if (id == 'rm-trending') {
        var addClass = document.getElementById("bio-sub-item-5");
        addClass.classList.add("active");
        var removeClass_1st = document.getElementById("bio-sub-item-1");
        removeClass_1st.classList.remove("active");
        var removeClass_2nd = document.getElementById("bio-sub-item-2");
        removeClass_2nd.classList.remove("active");
        var removeClass_3rd = document.getElementById("bio-sub-item-3");
        removeClass_3rd.classList.remove("active");
        var removeClass_4th = document.getElementById("bio-sub-item-4");
        removeClass_4th.classList.remove("active");
    } else {
        var addClass = document.getElementById("bio-sub-item-1");
        addClass.classList.add("active");
        var removeClass_1st = document.getElementById("bio-sub-item-2");
        removeClass_1st.classList.remove("active");
        var removeClass_2nd = document.getElementById("bio-sub-item-3");
        removeClass_2nd.classList.remove("active");
        var removeClass_3rd = document.getElementById("bio-sub-item-4");
        removeClass_3rd.classList.remove("active");
        var removeClass_4th = document.getElementById("bio-sub-item-5");
        removeClass_4th.classList.remove("active");
    }

}