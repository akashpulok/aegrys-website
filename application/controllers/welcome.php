<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    public function index()
    {
        $this->load->helper('url');
        $this->load->view('home');
    }

    public function send()
    {
        $mailto = "aegyriss@gmail.com";
        $password = "somrubio123";

        // $email = $this->input->post('email');
        // $phone = $this->input->post('phone_number');
        // $f_name = $this->input->post('f_name');
        // $l_name = $this->input->post('l_name');
        // $company = $this->input->post('company');
        // $country = $this->input->post('country');
        // $module = $this->input->post('module');
        // $component = $this->input->post('component');
        // $message = $this->input->post('message');

        $varData = $this->input->post('varData');

        $config['protocol'] = "smtp";
        $config['smtp_host'] = "smtp.gmail.com";
        $config['smtp_port'] = "587";
        $config['smtp_crypto'] = 'tls';
        $config['smtp_timeout'] = "400";
        $config['smtp_user'] = $mailto;
        $config['smtp_pass'] = $password;
        $config['validate'] = true;
        $config['charset'] = 'utf-8';
        // $config['mailtype'] = "html";
        $config['crlf'] = "\r\n";

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($mailto);
        $this->email->to($mailto);
        // $this->email->to('kafi.rashid@gmail.com');

        $this->email->subject('Someone wants to learn more about Aegyris');

        // $messageBody = "Received via Aegyris Contact Form" . "\r\n\r\n" .
        // "First name: " . $f_name . "\r\n" .
        // "Last name: " . $l_name . "\r\n" .
        // "Company: " . $company . "\r\n" .
        // "Country: " . $country . "\r\n" .
        // "Phone number: " . $phone . "\r\n" . 
        // "Email address: " . $email . "\r\n" .
        // "Module: " . $module . "\r\n" .
        // "Component: " . $component . "\r\n" .
        // "Message: " . $message;

        $this->email->message($varData);
        if ($this->email->send()) {
            // $message = "Sent";
            echo json_encode(array("success"));
        } else {
            // echo $this->email->print_debugger();
            json_encode(array("fail"));
        }
    }
}
