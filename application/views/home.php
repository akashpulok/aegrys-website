<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Aegyris</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="<?php echo base_url(); ?>assets/img/icons/favicon.png" rel="icon">

    <!-- Vendor CSS Files -->
    <link href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendor/aos/aos.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">

</head>

<body>

    <!-- ======= Header ======= -->
    <header id="header">
        <div class="container-fluid d-flex p2">

            <div class="logo float-left p-2 flex-shrink-1">
                <h1 class="text-light"><a href="#header" class="animated fadeInUp scrollto">
                        <img src="<?php echo base_url(); ?>assets/img/logo/Aegyris_Logo@2x.png" alt="Aegrys" />
                    </a></h1>
            </div>

            <nav class="nav-menu float-left d-none d-lg-block navbar-light p-2 w-100">
                <ul>
                    <li><a href="#about">About</a></li>
                    <li><a href="#bioanalytical">Bioanalytical</a></li>
                    <li><a href="#productivity">Productivity</a></li>
                    <li><a href="#art-analyzer">AI Engine</a></li>
                    <li><a href="#reporting">Reporting</a></li>
                    <li><a href="#audit">Audit & Complience</a></li>
                    <li><a href="#contact">Contact Us</a></li>
                    <!-- <a href="#contact" class="btn btn-info my-2 my-sm-0 float-right text-center btn-contact">Contact Us</a> -->
                </ul>

            </nav>

            <div class="logo float-right p-2 flex-shrink-1">
                <h1 class="text-light"><a href="#header" class="animated fadeInUp scrollto">
                        <img src="<?php echo base_url(); ?>assets/img/logo/Somru-Logo.jpg" alt="Aegrys" />
                    </a></h1>
            </div>


        </div>
    </header>
    <!-- End Header -->

    <!-- ======= Hero Section ======= -->
    <section id="hero">
        <div class="hero-container">
            <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

                <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

                <div class="carousel-inner" role="listbox">

                    <!-- Slide 1 -->
                    <div class="carousel-item active" style="background-image: url('<?php echo base_url(); ?>assets/img/slide/banner-1.jpg');">
                        <div class="row">
                            <div class="col-md-6 offset-md-2 carousel-container">
                                <div class="carousel-content container">
                                    <h2 class="animated fadeInDown">Comprehensive software platform for bioanalytical laboratory
                                        </span>
                                    </h2>
                                    <p class="animated fadeInUp">Your choice for streamlining data analysis, generating powerful reports and improving productivity while ensuring compliance requirements</p>
                                    <a href="#about" class="btn-get-started animated fadeInUp scrollto">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Slide 2 -->
                    <div class="carousel-item" style="background-image: url('<?php echo base_url(); ?>assets/img/slide/banner-2.jpg');">
                        <div class="row">
                            <div class="col-md-6 offset-md-2 carousel-container">
                                <div class="carousel-content container">
                                    <h2 class="animated fadeInDown">Automate method development and validation data analysis
                                    </h2>
                                    <p class="animated fadeInUp">Immunogenicity , PK, Biomarker, Biosimilar and Trending data analysis in step by step wizard based workflow</p>
                                    <a href="#bioanalytical" class="btn-get-started animated fadeInUp scrollto">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Slide 3 -->
                    <div class="carousel-item" style="background-image: url('<?php echo base_url(); ?>assets/img/slide/banner-3.jpg');">
                        <div class="row">
                            <div class="col-md-6 offset-md-2 carousel-container">
                                <div class="carousel-content container">
                                    <h2 class="animated fadeInDown">Improve laboratory productivity and increase capacity
                                    </h2>
                                    <p class="animated fadeInUp">Smart inventory, electronice worksheet and RFID tracking modules to increase laboratory productivity and increase capacity</p>
                                    <a href="#productivity" class="btn-get-started animated fadeInUp scrollto">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Slide 4 -->
                    <div class="carousel-item" style="background-image: url('<?php echo base_url(); ?>assets/img/slide/banner-4.jpg');">
                        <div class="row">
                            <div class="col-md-6 offset-md-2 carousel-container">
                                <div class="carousel-content container">
                                    <h2 class="animated fadeInDown">Implementation and Validation
                                    </h2>
                                    <p class="animated fadeInUp">Aegyris™ implements and validates 3x faster than industry norm</p>
                                    <!-- <a href="#implementation" class="btn-get-started animated fadeInUp scrollto">Read More</a> -->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon icofont-rounded-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon icofont-rounded-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>
        </div>
    </section>
    <!-- End Hero -->

    <main id="main">

        <!-- ======= About Us Section ======= -->
        <section id="about" class="about">
            <div class="container-fluid">

                <div class="row no-gutters">

                    <div class="col-lg-6 col-md-12 d-flex flex-column">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 offset-lg-2">
                                    <P class="title">About Aegyris™</P>
                                    <p class="summary">A Comprehensive software solution for the bioanalytical laboratory
                                    </p>

                                    <p class="about-content">Somru scientist have developed software solution to enable bioanalytical laboratories to meet rigorous scientific and regulatory requirements. Aegyris streamlines assay design and execution, data analysis and ensuring 21 CFR Part 11 compliance.</p>

                                    <p class="about-content">The web front-end of Aegyris provides a rich user interface for data analysis and visualization. It drives laboratory productivity, ensure compliance, and streamline processes by using simple easy-to-use intuitive wizard-based interface.
                                    </p>

                                    <p class="about-content">Aegyris™ is designed to enable bioanalytical scientists to perform method validation, statistical analysis, and validation workflow management in a streamlined and regulatory compliant manner. The IntelliLab module within Aegyris is designed to streamline day to day laboratory activities involving inventory, sample tracking, and electronic laboratory notebook (ELN).</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-12 d-flex flex-column about-content">

                        <div class="container">
                            <div class="row">
                                <div class="col-md-10">

                                    <p class="about-content-highlight">Aegyris™ can save up to 70% of time and costs in method validation support activities when implemented as designed.</p>
                                    <p class="about-content">A Comprehensive cloud based software solution for the bioanalytical laboratory.</p>
                                    <div class="about-content-list">
                                        <li>A Comprehensive software platform for bioanalytical laboratory</li>
                                        <li>Aegyris™ contains following functionalities:</li>
                                        <li style="margin-left: 30px;">Method Validations</li>
                                        <li style="margin-left: 60px;">PK, Immunogenicity, Biomarker (including multiplexing) and Biosimilar </li>
                                        <li style="margin-left: 30px;">Productivity Tools (IntelliLab)</li>
                                        <li style="margin-left: 60px;">Sample tracking using RFID</li>
                                        <li style="margin-left: 60px;">Inventory management</li>
                                        <li style="margin-left: 60px;">eLabEx (Electronic Lab notebook)</li>
                                        <li style="margin-left: 30px;">Artificial intelligence driven data analytics</li>
                                        <li>21 CFR Part 11 compliance</li>
                                        <li>Modules that are designed independently as micro-service for inter-operability</li>
                                        <li>Scalability</li>
                                        <li>Direct link to various LIMS or data system</li>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </section>
        <!-- End About Us Section -->

        <!-- ======= Bioanalytical Section ======= -->
        <section id="bioanalytical" class="bioanalytical">
            <div class="container-fluid">

                <div class="row no-gutters">
                    <div class="container">
                        <div class="row">

                            <div class="col-lg-12 col-md-12 d-flex flex-column">
                                <p class="title text-center">Aegyris Bioanalytical</p>

                                <div class="sub-content-1">
                                    <p class="sub-content-title">Biomarker</p>
                                    <p class="sub-content-text">Multiplexed biomarker assay data analysis accurately...
                                        <a href="#bio-sub" id="rm-biomarker" onclick="redirect('rm-biomarker')" class="b-rm animated fadeInUp scrollto">
                                            read more
                                        </a>
                                    </p>
                                    <a href="#contact" id="biomarket" onclick="get_quote('biomarket')" class="btn btn-outline-success animated fadeInUp scrollto text-center btn-biomarket">Contact Us</a>
                                </div>

                                <div class="sub-content-2">
                                    <p class="sub-content-title">PK - Pharmacokinetics</p>
                                    <p class="sub-content-text">Automated generation of validation experimental design including plate maps...
                                        <a href="#bio-sub" id="rm-pk" onclick="redirect('rm-pk')" class="b-rm animated fadeInUp scrollto">
                                            read more
                                        </a>
                                    </p>
                                    <a href="#contact" id="pk" onclick="get_quote('pk')" class="btn btn-outline-success animated fadeInUp scrollto text-center btn-pk">Contact Us
                                    </a>
                                </div>

                                <div class="sub-content-3">
                                    <p class="sub-content-title">Trending</p>
                                    <p class="sub-content-text">Proactive monitoring of assay performance and detecting potential assay issues before they fail...
                                        <a href="#bio-sub" id="rm-trending" onclick="redirect('rm-trending')" class="b-rm animated fadeInUp scrollto">
                                            read more
                                        </a>
                                    </p>
                                    <a href="#contact" id="trending" onclick="get_quote('trending')" class="btn btn-outline-success animated fadeInUp scrollto text-center btn-trending">Contact Us
                                    </a>
                                </div>

                                <div class="sub-content-4">
                                    <p class="sub-content-title">Immunogenicity - Anti Drug Antibodies</p>
                                    <p class="sub-content-text">Calculate immunogenicity cut points (screening, confirmatory and titer) in one click...
                                        <a href="#bio-sub" id="rm-ada" onclick="redirect('rm-ada')" class="b-rm animated fadeInUp scrollto">
                                            read more
                                        </a>
                                    </p>
                                    <a href="#contact" id="ada" onclick="get_quote('ada')" class="btn btn-outline-success animated fadeInUp scrollto text-center btn-ada">Contact Us
                                    </a>
                                </div>

                                <div class="sub-content-5">
                                    <p class="sub-content-title">Biosimilar</p>
                                    <p class="sub-content-text">Perform parallelism testing which is critical to establishing analytical similarity...
                                        <a href="#bio-sub" id="rm-biosimilar" onclick="redirect('rm-biosimilar')" class="b-rm animated fadeInUp scrollto">
                                            read more
                                        </a>
                                    </p>
                                    <a href="#contact" id="biosimilar" onclick="get_quote('biosimilar')" class="btn btn-outline-success animated fadeInUp scrollto text-center btn-biosimilar">Contact Us
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Bioanalytical Section -->

        <!-- ======= Bioanalytical Sub Section ======= -->
        <section class="bio-sub" id="bio-sub">

            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active bio-sub-item-1" id="bio-sub-item-1">
                        <div class="container h-100">
                            <div class="row h-100 justify-content-center align-items-center">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xl-6 carousel-container">
                                    <div class="carousel-content container">

                                        <div class="col-md-10">
                                            <p class="title">Aegyris™ Immunogenicity</p>
                                            <p class="sub-title">The Immunogenciity module is designed to streamline immunogenicity (also known as anti-drug antibodies assay) method validation. The user friendly assay workflow guides scientists to execute validation assays, perform data analysis and generate reports in a regulatory compliant manner. The powerful statistical engine allows for robust calculation of assay cutpoints (i.e. screening, confirmatory, and titer)</p>
                                            <p class="sub-title">The flexible configuration options allow scientists to comply with their internal standard operating procedures. The immunogenicity module includes the evaluation of the following assay parameters as per FDA/EMA Immunogenicity Method Validation Guidance.</p>
                                            <div class="content-list">
                                                <li>Documentation - Validation plan & report</li>
                                                <li>Screening Cut Point</li>

                                            </div>
                                            <br>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xl-6">
                                    <div class="carousel-content container">

                                        <div class="col-md-12">
                                            <div class="">
                                                <img src="<?php echo base_url(); ?>assets/img/bio-sub/ADA_component@2x.png" class="rounded sub-img-ada" />
                                            </div><br><br>
                                            <div class="content-list">
                                                <li>Confirmatory Cut Point</li>
                                                <li>Titer Cut point</li>
                                                <li>Precision and Acceptance Criteria</li>
                                                <li>Determination of Low Positive Control</li>
                                                <li>Sensitivity</li>
                                                <li>Hook Effect</li>
                                                <li>Selectivity</li>
                                                <li>Specificity</li>
                                                <li>Stability </li>
                                                <li>Summary Precision</li>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item bio-sub-item-2" id="bio-sub-item-2">
                        <div class="container h-100">
                            <div class="row h-100 justify-content-center align-items-center">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xl-6 carousel-container">
                                    <div class="carousel-content container">

                                        <div class="col-md-10">
                                            <p class="title">Aegyris™ PK</p>
                                            <p class="sub-title">The PK module is designed to streamline pharmacokinetic assay validation. The user-friendly assay workflow guides scientists in executing validation assays, performing data analysis, and generating reports in a regulatory compliant manner.</p>
                                            <p class="sub-title">The flexible configuration options allow scientists to comply with their internal standard operating procedures. The PK module includes evaluation of the following assay parameters as per FDA/EMA/ICH Bioanalytical Method Validation Guidance.</p>
                                            <div class="content-list">
                                                <li>Documentation - Validation plan & report</li>
                                                <li>Calibration Curve Model</li>
                                                <li>Accuracy and Precision</li>
                                                <li>Total Error</li>
                                                <li>Selectivity</li>
                                                <li>Specificity</li>
                                            </div>
                                            <br>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xl-6">
                                    <div class="carousel-content container">

                                        <div class="col-md-12">
                                            <div class="">
                                                <img src="<?php echo base_url(); ?>assets/img/bio-sub/Pyruvate_kinas_component@2x.png" class="rounded sub-img-pk" />
                                            </div><br><br>
                                            <div class="content-list">
                                                <li>Sensitivity</li>
                                                <li>Dilutional Linearity</li>
                                                <li>Short and Long term stability </li>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="carousel-item bio-sub-item-3" id="bio-sub-item-3">
                        <div class="container h-100">
                            <div class="row h-100 justify-content-center align-items-center">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xl-6 carousel-container">
                                    <div class="carousel-content container">

                                        <div class="col-md-10">
                                            <p class="title">Aegyris™ Biomarker</p>
                                            <p class="sub-title">The bioanalytical laboratories are increasingly asked to performed challenging method validation of biomarkers to support regulatory decision making. The importance of fit-for-purpose method validation in biomarker’s context of use was also highlighted in recent FDA bioanalytical Method Validation Guidance (2018).</p>
                                            <p class="sub-title">To meet the demand from the bioanalytical community, Somru BioScience has developed separate biomarker method validation module within Aegyris platform that meets the need of the industry and the intent of the global regulatory guidances. The Biomarker moldule allows for validation workflow management and data analysis for up to 10 analytes sultaneously.</p>
                                            <p class="sub-title">The biomarker method validation module offers following functionality. In addition, the trending module within Aegyris allows for trending of data enabling confirmation of assay performance, reagent suitability and endogenous analyte stability.</p>

                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xl-6">
                                    <div class="carousel-content container">

                                        <div class="col-md-12">
                                            <div class="">
                                                <img src="<?php echo base_url(); ?>assets/img/bio-sub/biomarker.png" class="rounded sub-img-ada" />
                                            </div><br><br>
                                            <div class="content-list">
                                                <li>Documentation - Validation plan & report</li>
                                                <li>Screening Cut Point</li>
                                                <li>Confirmatory Cut Point</li>
                                                <li>Titer Cut point</li>
                                                <li>Precision and Acceptance Criteria</li>
                                                <li>Determination of Low Positive Control</li>
                                                <li>Sensitivity</li>
                                                <li>Hook Effect</li>
                                                <li>Selectivity</li>
                                                <li>Specificity</li>
                                                <li>Stability </li>
                                                <li>Summary Precision</li>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item bio-sub-item-4" id="bio-sub-item-4">
                        <div class="container h-100">
                            <div class="row h-100 justify-content-center align-items-center">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xl-6 carousel-container">
                                    <div class="carousel-content container">

                                        <div class="col-md-10">
                                            <p class="title">Aegyris™ Biosimilar</p>
                                            <p class="sub-title">Biosimilar method validation presents unique challenges to bioanalytical scientists. In addition to ensuring that all the validation parameters mentioned in PK module, for Biosimilar method validation, bioanalytical scientists are also required to demonstrate that one method can measure both innovator and biosimilar in comparable manner. This requires sophisticated statistical measures and robust data analysis tools.</p>
                                            <p class="sub-title">The Aegyris Biosimilar module includes all the components of PK module. In addition, it also includes following parameters.</p>

                                            <br><br><br><br><br><br>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xl-6">
                                    <div class="carousel-content container">

                                        <div class="col-md-12">
                                            <div class="">
                                                <img src="<?php echo base_url(); ?>assets/img/bio-sub/ADA_component@2x.png" class="rounded sub-img-ada" />
                                            </div><br><br>
                                            <div class="content-list">
                                                <li>Standard curve comparison</li>
                                                <li>Quality Control (QC) Comparison</li>
                                                <li>Analytical Similarity Evaluation </li>
                                                <li>Statistical measures to confirm one assay approach</li>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item bio-sub-item-5" id="bio-sub-item-5">
                        <div class="container h-100">
                            <div class="row h-100 justify-content-center align-items-center">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xl-6 carousel-container">
                                    <div class="carousel-content container">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="title">Aegyris™ Trending</p>
                                                <p class="sub-title">Perform proactive monitoring of assay performance and detect potential assay issues before they fail using Aegyris™ Trending component.</p>
                                                <div class="content-list">
                                                    <li>Automated set up of Trend analysis reports - for a specific time range between two dates & times or for a dynamic time range (e.g. last 30 days etc)</li>
                                                    <li>Allows for comparisons among multiple variables. For example- between operators or lot of reagents etc.</li>
                                                    <li>Statistical analysis (e.g. ANOVA, regression analysis, correlation etc.) for entire dataset or subset of data</li>
                                                    <li>Accuracy and Precision</li>
                                                    <li>Transformations: producing a new time series from an existing one. Useful for biological data</li>
                                                    <li>Automated identification of root cause based on artificial intelligence</li>
                                                </div><br><br><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xl-6">
                                    <div class="carousel-content container">

                                        <div class="col-md-12">
                                            <div class="">
                                                <img src="<?php echo base_url(); ?>assets/img/bio-sub/Trending_component@2x.png" class="rounded sub-img-trending" />
                                            </div><br>
                                            <div class="content-list">
                                                <li>Warning of assay performance issues before the assay starts failing based on machine learning algorithms</li>
                                                <li>Automated link to LIMS system for routine upload</li>
                                                <li>Upcoming feature will include robust statistical approaches such as Curve-fitting/ growth curve, Filtering/Smoothing/ weighting, Kwiatkowski–Phillips–Schmidt–Shin (KPSS) test, MANCOVA (Multivariate Analysis of Covariance) , The Seasonal Kendall test (SK test) analyzes data for monotonic trends in seasonal data.</li>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Bioanalytical Sub Section -->

        <!-- ======= Productivity Section ======= -->
        <section class="productivity" id="productivity">
            <div class="container-fluid">

                <div class="row no-gutters">
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 offset-lg-2 child-1">
                                    <p class="title">Aegyris™ Productivity</p>
                                    <p class="sub-title"><img src="<?php echo base_url(); ?>assets/img/icons/Inventory@2x.png" alt="icon" height="15" width="15">&nbsp;&nbsp; Smart Inventory</p>
                                    <p class="content" id="p-txt-content">Smart real time inventory tracking and critical regent life cycle management...
                                        <a href="#contact" class="p-rm animated fadeInUp scrollto" class="p-rm">
                                            read more
                                        </a><br><br>
                                        <a href="#contact" id="inventory" onclick="get_quote('inventory')" class="btn btn-outline-success btn-p-get-quote animated fadeInUp scrollto text-center">Contact Us
                                        </a>
                                    </p>

                                    <p class="sub-title"><img src="<?php echo base_url(); ?>assets/img/icons/RFID_Sample_tracking@2x.png" alt="icon" height="15" width="15">&nbsp;&nbsp; RFID Sample tracking</p>
                                    <p class="content">Bioanalytical module contains 5 component (PK, Immunogenicity, Biomarker, Biosimilar, Trending) for streamlining...
                                        <a href="#contact" class="p-rm animated fadeInUp scrollto" class="p-rm">
                                            read more
                                        </a><br><br>
                                        <a href="#contact" id="rfid" onclick="get_quote('rfid')" class="btn btn-outline-success btn-p-get-quote animated fadeInUp scrollto text-center">Contact Us
                                        </a>
                                    </p>

                                    <p class="sub-title"><img src="<?php echo base_url(); ?>assets/img/icons/E-worksheets@2x.png" alt="icon" height="15" width="15">&nbsp;&nbsp; eLabEx
                                    </p>
                                    <p class="content">The electronic worksheet allows real time critical assay data capture and management backbone of any scientific experiment in your lab...
                                        <a href="#contact" class="p-rm animated fadeInUp scrollto" class="p-rm">
                                            read more
                                        </a><br><br>
                                        <a href="#contact" id="e-worksheet" onclick="get_quote('e-worksheet')" class="btn btn-outline-success btn-p-get-quote animated fadeInUp scrollto text-center">Contact Us
                                        </a>
                                    </p>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 content-image d-flex align-content-center flex-wrap">
                        <img src="<?php echo base_url(); ?>assets/img/Aegyris_Productivity@2x.png" class="rounded mx-auto d-block img-productivity" />
                    </div>

                </div>

            </div>
        </section>
        <!-- End Productivity Section -->

        <!-- ======= Art AI Engine Section ======= -->
        <section class="art-analyzer" id="art-analyzer">
            <div class="container-fluid">

                <div class="row no-gutters">
                    <div class="col-lg-6 col-md-12 col-sm-12 text-center content-image d-flex align-content-center flex-wrap">
                        <img src="<?php echo base_url(); ?>assets/img/art-analyzer.webp" class="rounded mx-auto d-block img-analyzer" />
                    </div>

                    <div class="col-lg-6 col-md-12 col-sm-12 content-text d-flex align-content-center flex-wrap">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 child">
                                    <p class="title">State of The Art AI Engine</p>
                                    <p class="content" id="art-txt-content">Bioanalytical module contains component streamlinin data analysis
                                    </p>
                                    <li>Statistical analysis engine</li>
                                    <li>Uses standard R library package</li>
                                    <li>R library packages being validated by multiple well recognized organization
                                    </li>
                                    <li>Communicates with Aegyris Base via ESB using HTTP API</li>
                                    <li>Automated identification of discrepancies between Validation plan and report</li>
                                    <li>Automated warning and error notification for deviation from regulatory guideline</li>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Art AI Engine Section -->

        <!-- section reporting start -->
        <section class="reporting" id="reporting">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 content text-center">
                        <p class="title">Powerful Reporting</p>

                        <p class="sub-title">Bioanalytical module contains 5 component (PK, Immunogenicity, Biomarker, Biosimilar, Trending) for streamlining data analysis.</p>
                    </div>

                    <div class="col-lg-6 col-md-12 col-sm-12 d-flex align-content-start flex-wrap">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 offset-lg-2 child">
                                    <p class="summary">Bioanalytical module contains 5 component (PK, Immunogenicity, Biomarker, Biosimilar, Trending) for streamlining.</p>

                                    <li>Report is configurable and customizable</li>
                                    <li>Val Report contains 3 types of data</li>
                                    <li style="margin-left: 20px;">Free form text</li>
                                    <li style="margin-left: 20px;">Non-editable template information (e.g. compliance statement)
                                    </li>
                                    <li style="margin-left: 20px;">Result table from Aegyris™ data analysis</li>
                                    <li>Will need data tables and plate maps to construct appropriate template</li>
                                    <li>Report can be exported in various format (e.g. MS word or PDF)</li>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 text-center content-image d-flex align-content-center flex-wrap">
                        <img src="<?php echo base_url(); ?>assets/img/report.png" class="rounded mx-auto d-block img-reporting" />
                    </div>
                </div>
            </div>

        </section>
        <!-- section reporting end -->



        <!-- section audit start -->
        <section class="audit" id="audit">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 content text-center">
                        <p class="title">Ready for Your Audit</p>

                        <p class="sub-title">The audit trail is automated, secure (can’t be edited), contemporaneous, traceable, and archived</p>
                    </div>

                    <div class="col-lg-6 col-md-12 col-sm-12 d-flex align-content-center flex-wrap">
                        <img src="<?php echo base_url(); ?>assets/img/report.png" class="rounded mx-auto d-block img-audit" />
                    </div>

                    <div class="col-lg-6 col-md-12 col-sm-12 d-flex align-content-center flex-wrap">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 child">
                                    <p class="summary">The audit trail is automated, secure (can’t be edited), contemporaneous, traceable, and archived</p>

                                    <li>The audit trail is automated, secure (can’t be edited), contemporaneous, traceable, and archived</li>
                                    <li>Audit trail content includes – identification of user, date and time stamp, record Id, original value, new value, reason for change and electronic signature
                                    </li>
                                    <li>Configurable data retention policy</li>
                                    <li>Archiving</li>
                                    <li style="margin-left: 20px;">Data is locked and stored in archive directory with limited access</li>
                                    <li style="margin-left: 20px;">Ability to load data on application with limited user access (read only) upon request and approval</li>
                                    <li style="margin-left: 20px;">Ability to integrate with third party document management software</li>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- section audit end -->

        <!-- ======= Contact us Section ======= -->
        <section class="contact" id="contact">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-lg-6 col-md-12 col-sm-12 d-flex align-content-center flex-wrap">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 offset-lg-2 child">
                                    <p class="title">CONTACT US</p>
                                    <p class="summary">Please drop us a note and we will get in touch shortly</p>

                                    <p class="contact-content">Want to learn more about Aegyris</p>
                                    <p class="contact-content">Want to find out how Aegyris can help your lab</p>
                                    <p class="contact-content">Want to get a quotation for any of the modules of Aegyris</p>
                                    <p class="contact-content">Get help with Aegryis or any of its module</p>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-12 col-sm-12 form">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <p class="form-title">We would love to hear from you</p>
                                </div>
                                <div class="col-md-12" style="margin-bottom: 100px;">
                                    <!--<div class="php-email-form">-->
                                    <form method="post" role="form" class="php-email-form" action="">
                                        <!--<form method="post" role="form" class="php-email-form" action="/welcome/send">-->
                                        <div class="form-row">
                                            <div class="col-lg-6 form-group">
                                                <input type="text" name="f_name" class="form-control" id="f_name" placeholder="First name" />
                                                <div class="validate"></div>
                                            </div>
                                            <div class="col-lg-6 form-group">
                                                <input type="text" class="form-control" name="l_name" id="l_name" placeholder="Last name" />
                                                <div class="validate"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Please enter a valid email" />
                                            <div class="validate"></div>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Phone Number" data-rule="minlen:11" data-msg="Please enter at least 11 digits of phone number" />
                                            <div class="validate"></div>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="company" id="company" placeholder="Company" />
                                            <div class="validate"></div>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control" name="country" id="country">
                                                <option value="Not set">Country</option>
                                                <option value="Afghanistan">Afghanistan</option>
                                                <option value="Albania">Albania</option>
                                                <option value="Belgium">Belgium</option>
                                                <option value="Bangladesh">Bangladesh</option>
                                                <option value="Belarus">Belarus</option>
                                                <option value="Comoros">Comoros</option>
                                                <option value="Cyprus">Cyprus</option>
                                                <option value="Grenada">Grenada</option>
                                                <option value="Kenya">Kenya</option>
                                                <option value="Malaysia">Malaysia</option>
                                                <option value="Mauritania">Mauritania</option>
                                                <option value="Mexico">Mexico</option>
                                                <option value="Mozambique">Mozambique</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control" name="module" id="module">
                                                <option value="Module">Module</option>
                                                <option value="Bioanalytical">Bioanalytical</option>
                                                <option value="Productivity">Productivity</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control" name="component" id="component">
                                                <option value="">Component</option>
                                                <option value="Immunogenicity - Anti Drug Antibodies">Immunogenicity - Anti Drug Antibodies</option>
                                                <option value="Biosimilar">Biosimilar</option>
                                                <option value="Trending">Trending</option>
                                                <option value="PK - Pharmacokinetics">PK - Pharmacokinetics</option>
                                                <option value="Biomarket">Biomarker</option>
                                                <option value="Smart Inventory">Smart Inventory</option>
                                                <option value="RFID">RFID</option>
                                                <option value="E-worksheets">eLabEx</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" name="message" id="message" rows="5" placeholder="Message"></textarea>
                                        </div>

                                        <div class="mb-3">
                                            <div class="message" style="color: white; text-align: center;"></div>
                                        </div>
                                        <div class="text-center"><button type="submit" class="btn btn-success btn-lg btn-block">Submit</button></div>
                                    </form>
                                    <!--</div>-->
                                </div>


                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- End contact us Section -->

        <!-- section resources start -->
        <section class="resources" id="resources">

            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 content text-center">
                        <p class="title">Resources</p>
                        <div id="resource-carousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner row w-100 mx-auto">
                                <!-- <div class="carousel-item col-lg-4 col-md-6 col-sm-12 active">
                                    <div class="card">
                                        <img src="<?php echo base_url(); ?>assets/img/resource-slide/1.JPG" class="card-img-top" width="350" height="250" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;">
                                        <div class="card-body">
                                            <h4 class="card-title">How similar is your biosimilars?</h4>
                                            <p class="card-text">Bioanalytical module contains component (PK, Immunogenicity, Biomarker, Biosimilar, Trending for streamlining data analysis Sadia Islam, a designer profession.</p>
                                            <a href="<?php echo base_url(); ?>assets/document/Aegyris Platform_Landing page_Feedback.pdf" role="button" class="btn btn-success" target="_blank" download>Download</a>

                                            <button type="button" href="#" data-toggle="modal" data-target="#r-content-1" class="btn btn-outline-success">Read
                                                More</button>
                                        </div>
                                    </div>
                                </div> -->
                                <!-- <div class="carousel-item col-lg-4 col-md-6 col-sm-12">
                                    <div class="card">
                                        <div class="embed-responsive embed-responsive-4by3">
                                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2Xa3Y4xz8_s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">Web Application Developer Summer Intern
                                            </h4>
                                            <p class="card-text">Bioanalytical module contains component (PK, Immunogenicity, Biomarker, Biosimilar, Trending for streamlining data analysis Sadia Islam, a designer profession.</p>

                                            <button type="button" href="#" data-toggle="modal" data-target="#r-content-2" class="btn btn-outline-success">Read
                                                More</button>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="carousel-item col-lg-4 col-md-6 col-sm-12 active">
                                    <div class="card">
                                        <img src="<?php echo base_url(); ?>assets/img/resource-slide/6.JPG" class="card-img-top" width="350" height="250" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;">
                                        <div class="card-body">
                                            <h4 class="card-title">Aegyris Brochure</h4>
                                            <p class="card-text">An Integrated Bioanalytical Method Validation Software for PK, Immunogenicity, Biomarker, and Biosimilar Assays </p><br>
                                            <a href="<?php echo base_url(); ?>assets/document/Respurce_1_Somru_Aegyris_Brochure.pdf" role="button" class="btn btn-outline-success" target="_blank" open>Read More</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item col-lg-4 col-md-6 col-sm-12">
                                    <div class="card">
                                        <img src="<?php echo base_url(); ?>assets/img/resource-slide/7.JPG" class="card-img-top" width="350" height="250" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;">
                                        <div class="card-body">
                                            <h4 class="card-title">Aegyris for Trending Biomarker Data</h4>
                                            <p class="card-text">Somru BioScience utilizes Aegyris for trending Biomarker data and improve method performance</p><br>
                                            <a href="<?php echo base_url(); ?>assets/document/Resource_3_Aegyris_Vaccine_Poster_FInal.pdf" role="button" class="btn btn-outline-success" target="_blank" open>Read More</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item col-lg-4 col-md-6 col-sm-12">
                                    <div class="card">
                                        <img src="<?php echo base_url(); ?>assets/img/resource-slide/4.JPG" class="card-img-top" width="350" height="250" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;">
                                        <div class="card-body">
                                            <h4 class="card-title">Vaccine Method Validation</h4>
                                            <p class="card-text">Somru BioScience utilizes Aegyris to streamline Vaccine method validation</p><br><br><br>
                                            <a href="<?php echo base_url(); ?>assets/document/Resource_2_Stability trending Bioanalysis article.pdf" role="button" class="btn btn-outline-success" target="_blank" open>Read More</a>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <!-- <a href="#resource-carousel" class="carousel-control-next next" data-slide="next">
                                <span class="carousel-control-next-icon button"></span>
                            </a> -->
                        </div>
                    </div>

                    <!-- The Modal for Resource -->
                    <div class="modal fade" id="r-content-1">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h4 class="modal-title">How similar is your biosimilars?</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <!-- Modal body -->
                                <div class="modal-body">
                                    <p>Bioanalytical module contains component (PK, Immunogenicity, Biomarker, Biosimilar, Trending for streamlining data analysis Sadia Islam, a designer profession.</p>
                                </div>

                                <!-- Modal footer -->
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!-- The Modal for Resource -->
                    <div class="modal fade" id="r-content-2">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h4 class="modal-title">Web Application Developer Summer Intern</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <!-- Modal body -->
                                <div class="modal-body">
                                    <p>Bioanalytical module contains component (PK, Immunogenicity, Biomarker, Biosimilar, Trending for streamlining data analysis Sadia Islam, a designer profession.</p>
                                </div>

                                <!-- Modal footer -->
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </section>
        <!-- section resources end -->

    </main>
    <!-- End #main -->

    <!-- ======= Footer ======= -->
    <footer id="footer">
        <div class="footer-top-bar">
            <div class="container">
                <div class="row">

                    <div class="col-lg-5 col-md-6 footer-info">
                        <h3>Need immediate <br> Assistance?</h3>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-info">
                        <h6><i class="icofont-phone" style="color: #88C242;"></i> (902) 367-4322</h6>
                        <p>Call us for free help</p>
                    </div>

                    <div class="col-lg-4 col-md-6 footer-info">
                        <h6><i class="icofont-email" style="color: #88C242;"></i> customer@somrubioscience.com</h6>
                        <p>Have a Question?</p>
                    </div>

                </div>
            </div>
        </div>

        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6 footer-info">
                        <h1 title="Aegyris">
                            <img src="<?php echo base_url(); ?>assets/img/logo/Aegyris_Logo@2x.png" alt="Aegyris" height="71" width="198" />
                        </h1>
                        <div class="social-links mt-3">
                            <a href="https://twitter.com/somrubio" class="twitter" title="twitter" target="_blank"><i class="bx bxl-twitter"></i></a>
                            <a href="https://www.facebook.com/Aegyris" class="facebook" title="Facebook" target="_blank"><i class="bx bxl-facebook"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Corporate Information</h4>
                        <ul>
                            <li><a href="http://www.somrubioscience.com" target="_blank">Somru BioScience Inc.</a></li>
                            <li><a href="#">Somru Blog</a></li>
                            <li><a href="http://www.somrubioscience.com/login" target="_blank">Signup to Somru</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Quick Links</h4>
                        <ul>
                            <li><a href="#header" class="animated fadeInUp scrollto">Aegrys Home</a></li>
                            <li><a href="#contact" class="animated fadeInUp scrollto">Get A Quote</a></li>
                            <li><a href="#">Blog</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Contact Address</h4>
                        <ul>
                            <li>53 Watts Ave. <br>
                                Charlottetown, PE <br>
                                Canada C1E 2B7
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>

        <div class="container">
            <div class="row copyright">
                <div class="col-md-12">
                    <p style="text-align: left;">Copyright &copy; 2020 All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

    <!-- Vendor JS Files -->
    <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/jquery.easing/jquery.easing.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/php-email-form/validate.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/jquery-sticky/jquery.sticky.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/waypoints/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/aos/aos.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!--Main JS File -->
    <script src="<?php echo base_url(); ?>assets/js/main.js"></script>

    <!-- google captcha api -->
    <script src="https://www.google.com/recaptcha/api.js?render=6LcKqO0UAAAAAKAPK1xftBhWUzgHFhVnc1sIw0KA"></script>

    <script>
        grecaptcha.ready(function() {
            grecaptcha.execute('6LcKqO0UAAAAAKAPK1xftBhWUzgHFhVnc1sIw0KA', {
                action: '/'
            });
        });

        $('.php-email-form').submit(function(ev) {
            ev.preventDefault();
        });

        // ajax stafs for form submit
        $(document).ready(function() {
            $('.php-email-form').on('submit', function() {
                // Add text 'loading...' right after clicking on the submit button. 
                // $('.loading').text('Loading...');

                // var email = $('#email').val();
                // var phone_number = $('#phone_number').val();
                // var f_name = $('#f_name').val();
                // var l_name = $('#l_name').val();
                // var l_name = $('#l_name').val();
                // var l_name = $('#l_name').val();
                // var l_name = $('#l_name').val();
                // var l_name = $('#l_name').val();
                // var l_name = $('#l_name').val();

                // var varData = 'email=' + email + '&phone_number=' + phone_number + '&f_name' + f_name + '&l_name' + l_name;
                let varData = "Received via Aegyris Contact Form" + "\r\n\r\n" +
                    "First name: " + $('#f_name').val() + "\r\n" +
                    "Last name: " + $('#l_name').val() + "\r\n" +
                    "Company: " + $('#company').val() + "\r\n" +
                    "Country: " + $('#country').val() + "\r\n" +
                    "Phone number: " + $('#phone_number').val() + "\r\n" +
                    "Email address: " + $('#email').val() + "\r\n" +
                    "Module: " + $('#module').val() + "\r\n" +
                    "Component: " + $('#component').val() + "\r\n" +
                    "Message: " + $('#message').val();

                console.log(varData);

                // var form = $(this);
                // console.log(form.serialize());
                $.ajax({
                    type: "POST",
                    url: "/welcome/send",
                    // async: false,
                    // crossDomain: true,
                    data: {
                        varData: varData
                    },
                    success: function(result) {
                        var a = JSON.parse(result);
                        // console.log(a[0]);
                        if (a[0] == 'success') {
                            console.log(result);
                            $('.message').text('Thank you for contacting us! We will get back to you shortly.');
                            $('.message').css('color', 'green');
                        } else {
                            // console.log(result);
                            $('.message').text('Something went wrong! Please try again later.');
                            $('.message').css('color', 'red');
                        }
                    }
                });
            });

            $(".sub-content-1").mouseover(function() {
                $(".btn-biomarket").css("display", "block");
            });
            $(".sub-content-1").mouseout(function() {
                $(".btn-biomarket").css("display", "none");
            });

            $(".sub-content-2").mouseover(function() {
                $(".btn-pk").css("display", "block");
            });
            $(".sub-content-2").mouseout(function() {
                $(".btn-pk").css("display", "none");
            });

            $(".sub-content-3").mouseover(function() {
                $(".btn-trending").css("display", "block");
            });
            $(".sub-content-3").mouseout(function() {
                $(".btn-trending").css("display", "none");
            });

            $(".sub-content-4").mouseover(function() {
                $(".btn-ada").css("display", "block");
            });
            $(".sub-content-4").mouseout(function() {
                $(".btn-ada").css("display", "none");
            });

            $(".sub-content-5").mouseover(function() {
                $(".btn-biosimilar").css("display", "block");
            });
            $(".sub-content-5").mouseout(function() {
                $(".btn-biosimilar").css("display", "none");
            });

        });
    </script>

</body>

</html>