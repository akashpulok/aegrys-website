<header id="header">
    <div class="container-fluid d-flex p2">

        <div class="logo float-left p-2 flex-shrink-1">
            <h1 class="text-light"><a href="#">
                    <img src="<?php echo base_url(); ?>assets/img/logo/Aegyris_Logo@2x.png" alt="Aegrys" />
                </a></h1>
        </div>

        <nav class="nav-menu float-left d-none d-lg-block navbar-light bg-light p-2 w-100">
            <ul>
                <li><a href="#about">About</a></li>
                <li><a href="#bioanalytical">Bioanalytical</a></li>
                <li><a href="#productivity">Productivity</a></li>
                <li><a href="#art-analyzer">Analyzer</a></li>
                <li><a href="#reporting">Reporting</a></li>
                <li><a href="#audit">Audit & Complience</a></li>
            </ul>
            <a href="#contact" class="btn btn-info my-2 my-sm-0 float-right text-center">Contact Us</a>
        </nav>


    </div>
</header>