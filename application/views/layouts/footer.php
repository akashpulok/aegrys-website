<footer id="footer">
    <div class="footer-top-bar">
        <div class="container">
            <div class="row">

                <div class="col-lg-6 col-md-6 footer-info">
                    <h3>Need immediate <br> Assistance?</h3>
                </div>

                <div class="col-lg-3 col-md-6 footer-info">
                    <h6><i class="icofont-phone" style="color: #88C242;"></i> (902) 367-4322</h6>
                    <p>Call us for free help</p>
                </div>

                <div class="col-lg-3 col-md-6 footer-info">
                    <h6><i class="icofont-email" style="color: #88C242;"></i> customer@aegyris.com</h6>
                    <p>Have a Question?</p>
                </div>

            </div>
        </div>
    </div>

    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6 footer-info">
                    <h1 title="Aegyris">
                        <img src="<?php echo base_url(); ?>assets/img/logo/Aegyris_Logo@2x.png" alt="Aegyris" height="71" width="198" />
                    </h1>
                    <div class="social-links mt-3">
                        <a href="#" class="twitter"><i class="bx bxl-twitter" title="twitter" target="_blank"></i></a>
                        <a href="https://www.facebook.com/Aegyris" class="facebook" title="Facebook" target="_blank"><i class="bx bxl-facebook"></i></a>
                        <a href="#" class="linkedin" title="Linkedin" target="_blank"><i class="bx bxl-linkedin"></i></a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Quick Link</h4>
                    <ul>
                        <li><a href="#">Immunogenicity Component</a></li>
                        <li><a href="#">Software demonstration</a></li>
                        <li><a href="#">Data Analyzer</a></li>
                        <li><a href="#">CFR Part Report</a></li>
                        <li><a href="#">Plan Clarification</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>More Products</h4>
                    <ul>
                        <li><a href="#">Antibodies</a></li>
                        <li><a href="#">Pharmacokinetic Kits</a></li>
                        <li><a href="#">Immunogenicity Kits</a></li>
                        <li><a href="#">Characterizstion Kits</a></li>
                        <li><a href="#">Biomarker Kits</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Company</h4>
                    <ul>
                        <li><a href="#">Contact us</a></li>
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Resource</a></li>
                        <li><a href="#">Terms & Licenses</a></li>
                        <li><a href="#">Help Center</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="row copyright">
            <div class="col-md-12">
                <p style="text-align: left;">Copyright &copy; 2020 All Rights Reserved.</p>
            </div>
        </div>
    </div>
</footer>